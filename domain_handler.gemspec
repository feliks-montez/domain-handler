Gem::Specification.new do |s|
  version = File.open('VERSION')
  s.name        = 'domain_handler'
  s.version     = version.read.gsub('\n','')
  s.date        = '2016-11-26'
  s.summary     = "a Ruby helper to allow automation of domain registrar functions"
  s.description = "currently only supports Freenom.com"
  s.authors     = ["Brant Meier"]
  s.email       = 'youngwebking@gmail.com'
  s.files       = Dir["{doc,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.rdoc", "VERSION"]
  s.homepage    =
    'http://rubygems.org/gems/hrecord'
    
  s.add_dependency "httparty"
  s.add_dependency "httmultiparty"
  
  s.license       = 'MIT'
  version.close
end
