module DomainHandler

  module APIErrors
  
    class CloudflareError < StandardError
      attr_accessor :errors, :codes
      
      def initialize(errors)
        @errors = {}
        @codes = []
        errors.each do |e|
          @errors[e['code']] = e['message']
          @codes << e['code']
        end
      end
      
      def to_s
        errors = @errors.map {|c,m| "#{c}: #{m}"}.join ';'
        "[#{errors}] #{super}"
      end
    
    end
  
  end

end
