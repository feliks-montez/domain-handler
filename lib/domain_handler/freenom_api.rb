module DomainHandler

  class FreenomAPI
  
    # format can be either 'json' or 'xml' for any of the API methods
    
    attr_accessor :default_format, :default_domaintype, :cookies
    
    def initialize(email:, password:, default_format: 'json', default_domaintype: 'FREE')
      @email    = email
      @password = password
      @api_uri  = "https://api.freenom.com/v2/"
      @default_format = default_format
      @default_domaintype = default_domaintype
    end
    
    def build_uri(path, format: @default_format)
      if format == 'json'
        uri = @api_uri + path
      elsif format == 'xml'
        uri = @api_uri + path + '.xml'
      end
      uri
    end
    
    ### Service Methods -------------------------------------------
    
    def ping(format: @default_format)
      uri = build_uri "service/ping", format: format
      HTTParty.get(uri)
    end
    
    ### Domain Methods -------------------------------------------
    
    def search(domainname:, format: @default_format)
      # domainname is full FQDN --> example.tk
      uri = build_uri "domain/search", format: format
      params = {
        domainname: domainname,
        domaintype: @default_domaintype
      }
      
      HTTParty.get(uri, query: params)
    end
    
    def register(domainname:, format: @default_format, nameservers: ['ns01.freenom.com','ns02.freenom.com','ns03.freenom.com','ns04.freenom.com'], period: '12M', autorenew: 'disabled', domaintype: @default_domaintype)
      uri = build_uri "domain/register", format: format
      # workaround to append nameservers to uri since hashes cannot have duplicate keys
      nameservers.each do |ns|
        uri += "#{ns == nameservers.first ? '?' : '&'}nameserver=#{ns}"
      end
      params = {
        email: @email,
        password: @password,
        domainname: domainname,
        domaintype: domaintype,
        period: period
      }
      
      HTTParty.post(uri, query: params)
    end
    
    def renew(domainname:, format: @default_format, period: '12M')
      uri = build_uri "domain/renew", format: format
      params = {
        email: @email,
        password: @password,
        domainname: domainname,
        period: period
      }
      
      HTTParty.post(uri, query: params)
    end
    
    def getinfo(domainname:, format: @default_format)
      uri = build_uri "domain/getinfo", format: format
      params = {
        email: @email,
        password: @password,
        domainname: domainname
      }
      
      HTTParty.get(uri, query: params)
    end
    
    def modify(domainname:, format: @default_format, nameservers: nil, period: nil)
      uri = build_uri "domain/modify", format: format
      # workaround to append nameservers to uri since hashes cannot have duplicate keys
      if !nameservers.empty?
        nameservers.each do |ns|
          uri += "#{ns == nameservers.first ? '?' : '&'}nameserver=#{ns}"
        end
      end
      params = {
        email: @email,
        password: @password,
        domainname: domainname,
        period: period
      }
      
      HTTParty.put(uri, query: params)
    end
    
    def getinfo(domainname:, format: @default_format)
      uri = build_uri "domain/getinfo", format: format
      params = {
        email: @email,
        password: @password,
        domainname: domainname
      }
      
      HTTParty.get(uri, query: params)
    end
    
    def delete(domainname:, format: @default_format)
      uri = build_uri "domain/delete", format: format
      params = {
        email: @email,
        password: @password,
        domainname: domainname
      }
      
      HTTParty.delete(uri, query: params)
    end
    
    def restore(domainname:, format: @default_format)
      uri = build_uri "domain/restore", format: format
      params = {
        email: @email,
        password: @password,
        domainname: domainname
      }
      
      HTTParty.post(uri, query: params)
    end
    
    def list(pagenr: 1, results_per_page: 25, format: @default_format)
      uri = build_uri "domain/list", format: format
      params = {
        email: @email,
        password: @password,
        pagenr: pagenr,
        results_per_page: results_per_page
      }
      
      HTTParty.get(uri, query: params)
    end
    
    ### Nameserver Methods -------------------------------------------
    
    def register_nameserver_glue(domainname:, hostname:, ipaddress:, format: @default_format)
      uri = build_uri "nameserver/register", format: format
      params = {
        email: @email,
        password: @password,
        domainname: domainname,
        hostname: hostname,
        ipaddress: ipaddress
      }
      
      HTTParty.put(uri, query: params)
    end
    
    def delete_nameserver_glue (domainname:, hostname:, format: @default_format)
      uri = build_uri "nameserver/delete", format: format
      params = {
        email: @email,
        password: @password,
        domainname: domainname,
        hostname: hostname
      }
      
      HTTParty.delete(uri, query: params)
    end
    
    def list_nameserver_glue_records(domainname:, format: @default_format)
      uri = build_uri "nameserver/list", format: format
      params = {
        email: @email,
        password: @password,
        domainname: domainname
      }
      
      HTTParty.get(uri, query: params)
    end

#    def create_subdomain(subdomain)
#      uri = "https://my.freenom.com/"
#      params = {
#        managedns: 'wormhole.ga',
#        domainid: 1020114531,
#        dnsaction: 'add',
#        token: '9073db19b07130d41c90ecad527195c0aa733f48',
#        :'addrecord[0][name]'  => subdomain,
#        :'addrecord[0][type]'  => 'cname',
#        :'addrecord[0][ttl]'   => 14440,
#        :'addrecord[0][value]' => 'nameserver.duckdns.org'
#      }
#      
#      # self.class has HTTParty's methods
#      response = HTTParty.get(uri, {query: params})
#      
#      return response
#    end
#    
#    def determine_subdomain(name: nil, fname: nil, lname: nil)
#      if !name.nil?
#        subdomain = name
#      elsif !fname.nil? and !lname.nil?
#        subdomain = fname + lname
#      else
#        raise ArgumentError.new("determine_subdomain requires either :name or :fname and :lname")
#      end
#      
#      return subdomain.downcase.gsub(/\W/, '')
#    end

  end

end
