module DomainHandler

  class CloudflareAPI
  
    ### Documentation: https://api.cloudflare.com/
  
    attr_accessor :api_url, :headers
    
    def initialize(email:, global_api_key:)
      @api_uri  = "https://api.cloudflare.com/client/v4"
      @headers = {
        "Content-Type": "application/json",
        "X-Auth-Key": global_api_key,
        "X-Auth-Email": email
      }
    end
    
    def build_uri(path)
      uri = @api_uri + path
      uri
    end
    
    
    ### Zone Methods -------------------------------------------
    
    def create_zone(name:, jump_start: nil, organization: nil)
      post "/zones", json: {
        name: name,
        jump_start: jump_start,
        organization: organization
      }
    end
    
    def zone_activation_check(id:)
      put "/zones/#{id}/activation_check"
    end
    
    def list_zones(name: nil, status: nil, page: nil, per_page: 20, order: nil, direction: nil, match: nil)
      get "/zones", params: {
        name: name, # "example.com" -- max length: 255
        status: status, # valid values: active, pending, initializing, moved, deleted, deactivated
        page: page, # default value: 1, min value: 1
        per_page: per_page, # min value: 5, max value: 100
        order: order, # valid values:  name, status, email
        direction: direction, # valid values: asc, desc
        match: match # Whether to match all search requirements or at least one (any). valid values: any, all
      }
    end
    
    def zone_details(id:)
      get "/zones/#{id}"
    end
    
    # Note: only one property can be changed at a time
    def edit_zone_property(id:, paused: nil, vanity_name_servers: nil, plan: nil)
      patch "/zones/#{id}", json: {
        paused: paused,
        vanity_name_servers: vanity_name_servers,
        plan: plan
      }
    end
    
    def purge_all_files(id:)
      delete "/zones/#{id}/purge_cache", json: {
        purge_everything: true
      }
    end
    
    def delete_zone(id:)
      delete "/zones/#{id}"
    end
    
    
    ### DNS Record Methods
    
    def create_dns_record(zone_id:, type:, name:, content:, ttl: 1.day.to_i, proxied: false)
      post "/zones/#{zone_id}/dns_records", json: {
        type: type, # "A" -- valid values: A, AAAA, CNAME, TXT, SRV, LOC, MX, NS, SPF
        name: name, # "example.com" -- max length: 255
        content: content, # "127.0.0.1"
        ttl: ttl, # Time to live for DNS record. Value of 1 is 'automatic'. min value: 1, max value: 2147483647
        proxied: proxied # false -- Whether the record is receiving the performance and security benefits of Cloudflare
      }
    end
    
    def list_dns_records(zone_id:, type: nil, name: nil, content: nil, page: 1, per_page: 20, order: nil, direction: nil, match: 'all')
      get "/zones/#{zone_id}/dns_records", params: {
        type: type, # "A" -- valid values: A, AAAA, CNAME, TXT, SRV, LOC, MX, NS, SPF
        name: name, # "example.com" -- max length: 255
        content: content, # "127.0.0.1"
        page: page,
        per_page: per_page, # min value: 5, max value: 100
        order: order, # valid values: type, name, content, ttl, proxied
        direction: direction, # valid values: asc, desc
        match: match # Whether to match all search requirements or at least one (any). valid values: any, all
      }
    end
    
    def dns_record_details(zone_id:, id:)
      get "/zones/#{zone_id}/dns_records/#{id}"
    end
    
    def update_dns_record(zone_id:, id:, type:, name:, content:, ttl: nil, proxied: nil)
      put "/zones/#{zone_id}/dns_records/#{id}", json: {
        ttl: ttl, # Time to live for DNS record. Value of 1 is 'automatic'. min value: 1, max value: 2147483647
        proxied: proxied # false -- Whether the record is receiving the performance and security benefits of Cloudflare
      }
    end
    
    def delete_dns_record(zone_id:, id:)
      delete "/zones/#{zone_id}/dns_records/#{id}"
    end
    
    def import_dns_records(zone_id:, file:)
      post "/zones/#{zone_id}/dns_records/import", params: {
        file: File.open(file) # BIND config to upload. "@bind_config.txt"
      }
    end
    
    
    ### Helper Methods -------------------------------------------
    
    def get(path, params: {}, json: {})
      process_params(params)
      res = HTTParty.get(build_uri(path), body: json.to_json, query: process_params(params), headers: @headers)
      parse_response(res)
    end
    
    def post(path, params: {}, json: {})
      res = HTTParty.post(build_uri(path), body: json.to_json, query: process_params(params), headers: @headers)
      parse_response(res)
    end
    
    def patch(path, params: {}, json: {})
      res = HTTParty.get(build_uri(path), body: json.to_json, query: process_params(params), headers: @headers)
      parse_response(res)
    end
    
    def put(path, params: {}, json: {})
      res = HTTParty.put(build_uri(path), body: json.to_json, query: process_params(params), headers: @headers)
      parse_response(res)
    end
    
    def delete(path, params: {}, json: {})
      res = HTTParty.delete(build_uri(path), body: json.to_json, query: process_params(params), headers: @headers)
      parse_response(res)
    end
    
    def process_params(params)
      new_params = {}
      params.each {|k,v| new_params[k] = v if v}
      new_params
    end
    
    def parse_response(res)
      #puts res.request.last_uri.to_s
      if res['success']
        res['result']
      elsif not res['errors'].empty?
        {'errors': res['errors']}
        raise APIErrors::CloudflareError.new res['errors']
      end
    end
    
  end
    
end
