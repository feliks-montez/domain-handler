module DomainHandler
  VERSION = File.read(File.join(File.dirname(__FILE__), "..", "VERSION")).chomp.freeze
end

require 'httparty'
require 'httmultiparty'

require File.join(File.dirname(__FILE__), "domain_handler", "freenom_api")
require File.join(File.dirname(__FILE__), "domain_handler", "cloudflare_api")
require File.join(File.dirname(__FILE__), "domain_handler", "cloudflare_errors")
require File.join(File.dirname(__FILE__), "domain_handler", "api_errors")
