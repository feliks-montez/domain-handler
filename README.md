# DomainHandler

A domain registrar API for Ruby

## Installation

- Obtain the latest version of the repository.
- Run ```gem build domain_handler.gemspec```
- Run ```gem install domain_handler-<version #>.gem```
- Done!
